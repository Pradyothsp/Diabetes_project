# Prediction of Diabetes 
In this project, the prediction of diabetes is done using 6 different algorithms namely 
- K-NN classifier
- Logistic Regression
- Decision Tree
- Random Forest
- Gradient Boosting
- Support Vector Machine (SVM) 

Here _flask_ is used as the framework for interaction with the frontend.
